﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;
using Ninject;
using Ninject.Web.Common;

namespace WebApplication5.Infrastructure
{
    //implement IDependencyResolver interface
    //used by MVC framework
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        //used to get an instance of a class
        //(service an incoming request)
        public object GetService(Type serviceType)
        {
            //returns null when no suitable binding
            return kernel.TryGet(serviceType);
        }

        //used to get an instance of a class
        public IEnumerable<object> GetServices(Type serviceType)
        {
            //supports multiple bindings
            //used when multiple implementation objects available
            return kernel.GetAll(serviceType);
        }

        //configure relationship between interface and LinqValue class
        private void AddBindings()
        {
            //InRequestScope = create one instance of LinqValueCalculator for each HTTP request that ASP.net receives
            kernel.Bind<IValueCalculator>().To<LinqValueCalculator>().InRequestScope();
            kernel.Bind<IValueCalculator>().To<LinqValueCalculator>();
            kernel.Bind<IDiscountHelper>().To<DefaultDiscountHelper>().WithConstructorArgument("discountParam", 50M);
            kernel.Bind<IDiscountHelper>().To<FlexibleDiscountHelper>().WhenInjectedInto<LinqValueCalculator>();
        }
    }
}