﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;


namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        private IValueCalculator calc;
        private Product[] products =
        {
            new Product {Name = "Kayak", Category = "Watersports", Price = 275M},
            new Product {Name = "Lifejacket", Category = "Watersports", Price = 48.95M},
            new Product {Name = "Soccer ball", Category = "Soccer", Price = 19.50M},
            new Product {Name = "Flag", Category = "Soccer", Price = 34.95M}
        };

        //Class constructor
        //Accepts implementation of the IValue interface, declares a dependency
        public HomeController(IValueCalculator calcParam, IValueCalculator calc2)
        {
            calc = calcParam;
        }

        // GET: Home
        public ActionResult Index()
        {
           /* //Ninject kernel object resolves dependencies
            IKernel ninjectKernel = new StandardKernel();

            //Configure the kernel so it knows which implementation to use for each interface
            ninjectKernel.Bind<IValueCalculator>().To<LinqValueCalculator>();

            //Use Ninject to create an object
            IValueCalculator calc = ninjectKernel.Get<IValueCalculator>();*/

            ShoppingCart cart = new ShoppingCart(calc) { Products = products };

            decimal totalValue = cart.CalculateProductTotal();

            return View(totalValue);
        }
    }
}