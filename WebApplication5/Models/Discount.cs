﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    //define ApplyDiscount method
    public interface IDiscountHelper
    {
        decimal ApplyDiscount(decimal totalParam);
    }

    //implement interface
    public class DefaultDiscountHelper : IDiscountHelper
    {
        public decimal discountSize;

        //Size of discount is passed as a constructor parameter
        public DefaultDiscountHelper(decimal discountParam)
        {
            discountSize = discountParam;
        }

        public decimal ApplyDiscount(decimal totalParam)
        {
            //10% fixed discount
            return (totalParam - (discountSize / 100M * totalParam));
        }
    }
}