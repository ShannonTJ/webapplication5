﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    //implement the calculator interface
    public class LinqValueCalculator: IValueCalculator
    {
        private IDiscountHelper discounter;
        private static int counter = 0;

        public LinqValueCalculator(IDiscountHelper discountParam)
        {
            discounter = discountParam;
            System.Diagnostics.Debug.WriteLine(
                string.Format("Instance {0} created", ++counter)
                );
        }

        public decimal ValueProducts(IEnumerable<Product> products)
        {
            //uses LINQ sum to add the prices of all Products in an enumerable
            return discounter.ApplyDiscount(products.Sum(p => p.Price));
        }
    }
}