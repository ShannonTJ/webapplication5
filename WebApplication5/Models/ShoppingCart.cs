﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class ShoppingCart
    {
        private IValueCalculator calc;

        public ShoppingCart(IValueCalculator calcParam)
        {
            calc = calcParam;
        }

        //collection of Product objects
        public IEnumerable<Product> Products { get; set; }

        //LINQ calculator gets total value of cart items
        public decimal CalculateProductTotal()
        {
            return calc.ValueProducts(Products);
        }
    }
}